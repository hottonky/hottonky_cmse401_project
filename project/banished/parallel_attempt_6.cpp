#include <iostream>
using namespace std;
#include <vector>
#include <string>
#include <map>


tuple<vector<string>, int> get_plan(int curr_time, int index, map<int, vector<float>>times, vector<int> priority, vector<string> rides, vector<string>& best_plan, int& points, int close_time, vector<string>& plan, int& max_points){
    // cout<< "Pounts:" << points << "  "<< endl;
    // cout<<"Current Path: ";
    // for (auto x:plan){
    //     cout<<x<< ", ";
    // }
    // cout<<endl;
    // cout<<"max_points: "<<max_points;
    // cout<<endl;
    // cout<<endl;
    if (curr_time < close_time){
        index =0;
        for (auto end_time:times[curr_time]){
            points += priority[index]; //add points of this ride
            string to_add =  to_string(curr_time)+ ":" + rides[index];
            plan.push_back(to_add); //add to current path
            get_plan(end_time, index, times, priority, rides, best_plan, points, close_time, plan, max_points); 
            points -= priority[index]; //going backwards so remove the last points added
            plan.pop_back(); //going backwards so remove the last ride
            index++;
        }
        
    }
    else{ //hit end of day
        if (points>max_points){ //check if new path found is better than current best
            best_plan = plan;
            max_points = points;
        }
    }
    
    return {best_plan, max_points};
    
}
    

int main(){
    map<int, vector<float>> times;
    times[9]  = {10.1, 10.2, 10.3, 10.4, 13.5, 10.6, 10.7, 15.8};
    times[10] = {11.1, 11.2, 12.3, 11.4, 11.5, 11.6, 11.7, 11.8};
    times[11] = {12.1, 12.2, 12.3, 12.4, 12.5, 12.6, 12.7, 12.8};
    times[12] = {13.1, 13.2, 13.3, 13.4, 13.5, 13.6, 13.7, 13.8};
    times[13] = {16.1, 14.2, 14.3, 14.4, 14.5, 14.6, 14.7, 17.8};
    times[14] = {15.1, 15.2, 15.3, 15.4, 15.5, 15.6, 15.7, 15.8};
    times[15] = {16.1, 16.2, 16.3, 16.4, 16.5, 16.6, 16.7, 16.8};
    times[16] = {17.1, 17.2, 17.3, 17.4, 17.5, 17.6, 17.7, 17.8};
    times[17] = {18.1, 18.2, 18.3, 18.4, 18.5, 18.6, 18.7, 18.8};
    times[18] = {19.1, 19.2, 19.3, 19.4, 19.5, 19.6, 19.7, 19.8};
    times[19] = {20.1, 20.2, 20.3, 20.4, 20.5, 20.6, 20.7, 20.8};
    times[20] = {21.1, 21.2, 21.3, 21.4, 21.5, 21.6, 21.7, 21.8};
    times[21] = {22.1, 22.2, 22.3, 22.4, 22.5, 22.6, 22.7, 22.8};

    int close_time = 18;
    int curr_time = 9;
    vector<string> rides = {"a", "b", "c", "d", "e", "f", "g", "h"};
    vector<int> priority = { 1,   1,   100 ,  10 ,  1 ,  1000 ,  1 ,  1};
    int points = 0;
    int max_points = 0;
    vector<string> plan;
    vector<string> best_plan;
    int index = 0;
    // for (int index = 0; index < rides.size(); index++){
    //     int i = index;
    //     best_plan = get_plan(int(times[9][i]), 0, times, priority, rides, best_plan={"9:"+ rides[i]}, points, close_time, plan={"9:"+ rides[i]});
    //         for (auto x:best_plan){
    //         cout<<x<<", ";
    //     }
    //     cout<<endl;
    // }
    vector<vector<string>> all_best_paths;
    vector<int> all_max_points;
    #pragma omp parallel num_threads(8) private (plan, points, best_plan, max_points)
	#pragma omp for schedule(auto)
    for (int i = 0; i < rides.size(); i++){
        max_points=0;
        points=0;
        tie(best_plan, max_points) = get_plan(int(times[9][i]), 0, times, priority, rides, best_plan={"9:"+ rides[i]}, points=priority[i], close_time, plan={"9:"+ rides[i]}, max_points);
        all_best_paths.push_back(best_plan);
        all_max_points.push_back(max_points);
        // for (auto x:best_plan){
        //     cout<<x<<", ";
        // }
        // cout << max_points << endl;

    }

    int final_points = 0;
    vector<string> final_path;
    for(int j = 0; j < rides.size(); j++){
        if (all_max_points[j] > final_points){
            final_points = all_max_points[j];
            final_path = all_best_paths[j];
        }
    }

    for (auto x:final_path){
        cout<<x<<", ";
    }
    

    // int i = 1;
    // best_plan = get_plan(int(times[9][i]), 0, times, priority, rides, best_plan={"9:"+ rides[i]}, points=priority[i], close_time, plan={"9:"+ rides[i]}, max_points);
    //     for (auto x:best_plan){
    //     cout<<x<<", ";
    // }
    // cout<<endl;

    // i = 2;
    // max_points=0;
    // points=0;
    // vector<string> best_plan2;
    // best_plan2 = get_plan(int(times[9][i]), 0, times, priority, rides, best_plan={"9:"+ rides[i]}, points, close_time, plan={"9:"+ rides[i]}, max_points);
    //     for (auto x:best_plan2){
    //     cout<<x<<", ";
    // }
}