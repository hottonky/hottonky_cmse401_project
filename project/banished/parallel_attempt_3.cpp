#include <iostream>
using namespace std;
#include <vector>
#include <string>
#include <map>
#include<tuple>
#include "omp.h"



tuple <vector<string>, int> get_plan(int curr_time, int index, map<int, vector<float>>times, vector<int> priority, vector<string> rides, vector<string>& best_plan, int& points, int close_time, vector<string>& plan, int& max_points){
    if (curr_time < close_time){
        index =0;
        for (auto end_time:times[curr_time]){
            points += priority[index]; //add points of this ride
            string to_add =  to_string(curr_time)+ ":" + rides[index];
            plan.push_back(to_add); //add to current path
            get_plan(end_time, index, times, priority, rides, best_plan, points, close_time, plan, max_points); 
            points -= priority[index]; //going backwards so remove the last points added
            plan.pop_back(); //going backwards so remove the last ride
            index++;
        }
        
    }
    else{ //hit end of day
        if (points>max_points){ //check if new path found is better than current best
            best_plan = plan;
            max_points = points;
        }
    }
    tuple <vector<string>, int> return_thing;
    return_thing = make_tuple(best_plan, max_points);
    return return_thing;
    
}
    

int main(){
    map<int, vector<float>> times_9;
    times_9[9]  = {10.1, 10.2, 10.3, 10.4, 10.5, 10.6, 10.7, 10.8};
    times_9[10] = {11.1, 11.2, 12.3, 11.4, 11.5, 11.6, 11.7, 11.8};
    times_9[11] = {12.1, 12.2, 12.3, 12.4, 12.5, 12.6, 12.7, 12.8};
    times_9[12] = {13.1, 13.2, 13.3, 13.4, 13.5, 13.6, 13.7, 13.8};
    times_9[13] = {16.1, 14.2, 14.3, 14.4, 14.5, 14.6, 14.7, 17.8};
    times_9[14] = {15.1, 15.2, 15.3, 15.4, 15.5, 15.6, 15.7, 15.8};
    times_9[15] = {16.1, 16.2, 16.3, 16.4, 16.5, 16.6, 16.7, 16.8};
    times_9[16] = {17.1, 17.2, 17.3, 17.4, 17.5, 17.6, 17.7, 17.8};
    times_9[17] = {18.1, 18.2, 18.3, 18.4, 18.5, 18.6, 18.7, 18.8};
    times_9[18] = {19.1, 19.2, 19.3, 19.4, 19.5, 19.6, 19.7, 19.8};
    times_9[19] = {20.1, 20.2, 20.3, 20.4, 20.5, 20.6, 20.7, 20.8};
    times_9[20] = {21.1, 21.2, 21.3, 21.4, 21.5, 21.6, 21.7, 21.8};
    times_9[21] = {22.1, 22.2, 22.3, 22.4, 22.5, 22.6, 22.7, 22.8};

    map<int, vector<float>> times_10;
    times_10[10] = {11.1, 11.2, 12.3, 11.4, 11.5, 11.6, 11.7, 11.8};
    times_10[11] = {12.1, 12.2, 12.3, 12.4, 12.5, 12.6, 12.7, 12.8};
    times_10[12] = {13.1, 13.2, 13.3, 13.4, 13.5, 13.6, 13.7, 13.8};
    times_10[13] = {16.1, 14.2, 14.3, 14.4, 14.5, 14.6, 14.7, 17.8};
    times_10[14] = {15.1, 15.2, 15.3, 15.4, 15.5, 15.6, 15.7, 15.8};
    times_10[15] = {16.1, 16.2, 16.3, 16.4, 16.5, 16.6, 16.7, 16.8};
    times_10[16] = {17.1, 17.2, 17.3, 17.4, 17.5, 17.6, 17.7, 17.8};
    times_10[17] = {18.1, 18.2, 18.3, 18.4, 18.5, 18.6, 18.7, 18.8};
    times_10[18] = {19.1, 19.2, 19.3, 19.4, 19.5, 19.6, 19.7, 19.8};
    times_10[19] = {20.1, 20.2, 20.3, 20.4, 20.5, 20.6, 20.7, 20.8};
    times_10[20] = {21.1, 21.2, 21.3, 21.4, 21.5, 21.6, 21.7, 21.8};
    times_10[21] = {22.1, 22.2, 22.3, 22.4, 22.5, 22.6, 22.7, 22.8};

    int close_time = 16;
    int curr_time = 9;
    int max_points = 0;
    vector<string> rides = {"a", "b", "c", "d", "e", "f", "g", "h"};
    vector<int> priority = { 1,   1,   2 ,  1 ,  1 ,  1 ,  1 ,  5};
    int points = 0;
    vector<string> plan;
    vector<string> best_plan;
    int index = 0;
    vector<string> temp_vec = {"temp"};
    tuple<vector<string>, int> temp_tuple(temp_vec, 0);
    vector<tuple<vector<string>, int>> all(8, temp_tuple);
    //vector<tuple<vector<string>, int>> all;
    #pragma omp parallel num_threads(8) private (plan, points, best_plan, max_points)
	#pragma omp for schedule(auto)
    for(int c=0;c<rides.size();c++) {
        plan.clear();
        points = priority[c]; //add points of this ride
        string to_add =  "9:" + rides[c];
        plan = {to_add};
        //plan.push_back(to_add); //add to current path
        tuple<vector<string>, int> returned_thing = get_plan(int(times_9[9][c]), index, times_10, priority, 
        rides, best_plan, points, close_time, plan, max_points);
        all[c]= make_tuple(get<0>(returned_thing),get<1>(returned_thing));
        // vector<string> best = get<0>(returned_thing);
        // cout<<rides[c]<<endl;
        // for (auto x:best){
        //     cout<<x<<", ";
        // }
        // cout << endl;
        //all.push_back(returned_thing);
        // cout<< "Here: " <<  get<0>(all[0])[0] << " : " << get<1>(all[0]) <<endl;
    }
    // int max_val=0;
    // vector<string> best;
    // for (auto y:all){
    //     if (get<1>(y) > max_val){
    //         max_val=get<1>(y);
    //         best = get<0>(y);
    //     }
    // }

    // for (auto y:all){
    //     best = get<0>(y);
    //         for (auto x:best){
    //         cout<<x<<", ";
    //     }
    //     cout << endl;
    // }

    int max_val=0;
    vector<string> best;
    for (auto y:all){
        if (get<1>(y) > max_val){
            max_val=get<1>(y);
            best = get<0>(y);
        }
    }
    for (auto x:best){
        cout<<x<<", ";
    }

}